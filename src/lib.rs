//! Global allocator for Rust no_std projects on newlib targets.

#![no_std]
#![warn(missing_docs)]

extern crate core;

use core::alloc::{GlobalAlloc, Layout};
use libc::{free, memalign};

/// Global allocator for Rust no_std projects on newlib targets.
pub struct Alloc;

unsafe impl GlobalAlloc for Alloc {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        memalign(layout.align(), layout.size()) as *mut _
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        free(ptr as *mut _);
    }
}
